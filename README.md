# 前端笔记

## 工具和资源

### [uTools](http://www.u.tools/) 
- 新一代效率工具平台，自由组合插件应用，打造专属你的趁手工具集

### apng
- png动画格式，替代gif 
- [在线工具](http://littlesvr.ca/apng/assembler/assembler.php)
- [iSparta软件](http://isparta.github.io/)

## 浏览器插件
### Clearly Reader
- 补充chrome没有的阅读模式，可以复制不让复制的网页内容

### Vue.js devtools
- vue插件，浏览器开发模式下可以查看vue的变量等

## vscode
### indent-rainbow
- 代码缩进用背景色区分

### Dot Log
- console.log快速输入
```js
// 输入 
111.cl
// 得到
console.log('111',111);
```

### HTML-Comment
- html多层注释

### Tabnine（Codota）
- 智能代码提示

### Copilot
- AI代码提示

### Prettier
- 格式化代码，在插件设置里配置，或根目录可放配置文件
- 配置文件 .prettierrc.js
- 快捷键 CMD/CTRL + Shift + P

## HTML

### oncontextmenu
- 网页禁用右键菜单
```html
<body oncontextmenu="return false"></body>
```

### video controlslist ...
- video标签禁用下载选项、网页内播放
- controlslist="nodownload"
- playsinline, webkit-playsinline 网页内播放，不自动全屏
- x5-video-player-type="h5-page"微信的同层页面内播放 [腾讯浏览服务官方文档](https://x5.tencent.com/docs/video.html)
```html
<video src="v.mp4" poster="v.jpg" controls="controls" width="100%" height="100%" preload="auto" playsinline="" x-webkit-airplay="" webkit-playsinline="" x5-video-player-type="h5-page" style="display: block;" controlslist="nodownload"></video>
```

### input弹出键盘显示搜索按钮
- 安卓上把input的type="search"即可，ios需要写form的action
- 点击搜索按钮会打开search.html?keywords=value
```html
<form action="search.html" method="get">
    <input name="keywords" class="search" type="search" placeholder="">
</form>
```
- js监听搜索按钮点击事件
```html
<form action="javascript:void 0">
    <input name="keywords" class="search" type="search" placeholder="">
</form>
```
```js
document.querySelector(".search").addEventListener("keypress",function(event) {
    if(event.keyCode == "13") {
        document.activeElement.blur();//收起虚拟键盘
        toSearch();//TODO 完成搜索事件
        event.preventDefault(); // 阻止默认事件---阻止页面刷新
    }
});
```
- vue写法
```html
<form class="search-block" action="javascript:void 0">
    <input type="text" v-model="txtInput" @keyup.13="tapToSearch" @input="tapToSearch" >
</form>
```
- 移除搜索框的叉叉图标
```css
::-webkit-search-cancel-button { display: none;}；
```
- enterkeyhint 确认键显示为不同文字 [说明](https://www.zhangxinxu.com/wordpress/2020/10/html-enterkeyhint-ios-iphone/)
```html
<form action="javascript:void 0">
    <input class="dm-input" type="text" enterkeyhint="send" placeholder="">
</form>
```
关键字值|描述
:---:|:---:
enter|‘enter’表示回车，多出现在textarea文本域等需要多行输入的场景中。
done|	‘done’表示完成，表示没有更多内容输入，输入结束。
go| ‘go’表示前往，, 意思是把用户带到他们输入的文本的目标处。
next|	‘next’表示下一项，通常会移动到下一个输入项那里。
previous|	‘previous’表示上一个，通常会移动到上一个输入项那里。
search|	‘search’表示搜索，通常用在搜索行为中。
send|	‘send’表示发，通常用在文本信息的发送上。

## CSS

### backdrop-filter
- 动态模糊下层元素
- backdrop-filter: blur(10px); 
- safari需要-webkit-backdrop-filter: blur(10px);
- 为了看到下层元素的滤镜效果，上层元素需要有背景色并设置透明度，否则会被完全挡住看不到
- backdrop-filter不止支持模糊（blur），filter支持的效果基本都支持
- [说明文档](https://developer.mozilla.org/zh-CN/docs/Web/CSS/backdrop-filter)
```css
backdrop-filter: blur(10px);
-webkit-backdrop-filter: blur(10px);
background-color: rgba(0,0,0,0.6);
```

### @supports
- 判断浏览器是否支持某个css 
- [说明文档](https://developer.mozilla.org/zh-CN/docs/Web/API/CSS/supports)
```css
@supports ((-webkit-backdrop-filter: blur()) or(backdrop-filter: blur())) {
    .header .header-bar {
        background-color: rgba(0, 0, 0, .5);
        -webkit-backdrop-filter: blur(12px);
        backdrop-filter:blur(12px)
    }
}
```
```js
// js也支持此判断
if(CSS.supports('display', 'grid')){
    alert('it support!')
}
```

### aspect-ratio
- 锁定长宽比
```css
aspect-ratio: 1/1;
```

### position: sticky
- 粘性定位，页面滚动到某元素时固定位置，不随页面滚动
- overflow: hidden会使此属性失效
```css
position: sticky;
top: 0;
```

### animation-play-state
- 暂停或播放动画动画
```css
/* 鼠标放上去后动画暂停 */
.ani{ animation: fadeIn 1s linear infinite;}
.ani:hover{ animation-play-state: paused;}

/* 鼠标放上去后动画播放 */
.ani{ animation: fadeIn 1s linear paused infinite;}
.ani:hover{ animation-play-state: running;}
```

### scroll-behavior
- 滚动框指定滚动行为
- js控制页面或元素滚动时，用smooth属性能以动画形式滚动到目标位置
```css
scroll-behavior:auto; // 滚动条立即滚动
scroll-behavior:smooth; // 窗口平稳滚动
scroll-behavior:inherit;
scroll-behavior:initial;
scroll-behavior:unset;
```

### transition-duration
- 元素属性改变以动画过度（css或js改变元素属性时不会立即到结果，而是动画过度到目标属性，可以替代js定时器来做动画）
```css
transition-duration: 1s;
```

### @media prefers-color-scheme
- dark：浏览器暗色模式
- light：浏览器亮色模式
```css
@media (prefers-color-scheme: dark) {
    body {
      color: white;
      background-color: black;
    }
}
```

### css变量
```html
<div style="--left: 100px">
```
```js
// js改变变量的值
document.querySelector('.left').style.setProperty('--left', '200px');
```
```css
/* 使用html的变量 */
.test{
    width: var(--left);
}
/* css定义和使用变量，要改变量的值，重复定义此css即可，ccs后定义的会覆盖前面定义的 */
:root{
    --blue: #0000ff;
}
.text{
    color: var(--blue);
}

```

### rem px 1:1
- [示例文件：rem.scss](./rem.scss)

### px替换成rem插件，可格式化ps复制的css
- [vscode插件：px-rem-0.0.1.vsix](./px-rem-0.0.1.vsix)


## JS
### IntersectionObserver
- 监听元素是否显示到视窗
```js
var intersectionObserver = new IntersectionObserver(function (entries) {
    // 如果不可见，就返回
    if (entries[0].intersectionRatio <= 0) return;
    // 可见就执行
    console.log("Loaded new items");
});

// 开始观察
intersectionObserver.observe(document.querySelectorAll('.element'));
```

### scrollIntoView
- 页面滚动到指定元素位置
```js
var e = document.querySelector('.footer');
e.scrollIntoView(); // 页面滚动到元素所在的位置

// start：元素显示在浏览器窗口顶部，end：元素显示在浏览器窗口底部，center: 中间，inline是水平方向，smooth：平滑滚动
e.scrollIntoView({block: "start", inline: "center", behavior: "smooth"});
```
### jq判断页面或元素滚动方向
```js
$(document).ready(function(){
        $(window).on("scroll",function(){
            //记录开始滚动位置
            var before=$(this).scrollTop();
            $(window).on("scroll",function(){
                //记录滚动之后的滚动位置变化
                var after=$(this).scrollTop();
                if(before>after) {
                    console.log("向上滚");
                    //把当前的滚动位置赋值给起始滚动位置
                    before=after;
                }else if(after>before){
                    console.log("向下滚");
                    //把当前的滚动位置赋值给起始滚动位置
                    before=after;
                }else {
                    console.log("error");
                }
            })
        })
    })
```

### getBoundingClientRect()
- 获取元素的xy坐标、长宽、四周边距
```js
document.querySelector('.time').getBoundingClientRect()
/*
返回数据
{
    bottom: 405.75,
    height: 31.5,
    left: 148.5,
    right: 224,
    top: 374.25,
    width: 75.5,
    x: 148.5,
    y: 374.25
*/
```
### 监听动画结束
```js
document.querySelector('.time').adaddEventListener('animationend', ()=>{
    console.log('动画播放结束');
})
```

## 资源
### [obfuscator](https://obfuscator.io/)
- 压缩混淆加密js

### [Electron](https://www.electronjs.org/)
- 使用 JavaScript，HTML 和 CSS 构建跨平台的桌面应用程序

### [Tauri](https://tauri.app/)
- 类似Electron，轻量化

### babel es6转es2015
- 安装 npm install -g babel-cli
- Babel的配置文件是.babelrc，存放在项目的根目录下，该文件用来设置转码规则和插件，基本格式如下。
```json
{
    "presets": ["es2015"],
    "plugins": []
}
```
- npm install --save-dev babel-preset-es2015
- 转换命令 npm ***.js -o ***.min.js

### babel官网在线es6转es2015
- https://babeljs.io/repl
- 左边配置勾选 LOOSE 和 Force All Transforms
