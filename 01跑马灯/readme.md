# 文字跑马灯效果 

## 调用方法 new lzPmd(selectors, option)
```js
const pmd = new lzPmd('.pmd', [{title:'', ulr:''}])
```
* selectors为class或id
* option为数组时，直接把数据列表传进来
* option为对象时，如下：
```js
{
    data: [{title: '标题', url: '链接地址'}], // 必选 url为空代表无链接
    target: '', // 可选 链接打开方式，_blank为新窗口，_self为当前窗口，默认为_blank，同a标签的target属性
    time: 20, // 可选 运动速度，单位毫秒，默认20，当方向为垂直滚动时，默认时间为3000毫秒（垂直滚动时此时间值必须大于1000毫秒，因为滚动动画持续时间是1秒）
    color: '#000', // 可选 a链接默认颜色#000，a:active也默认此颜色
    hover: '#f00', // 可选 a:hover颜色
    direction: 'horizontal' //可选，默认滚动为水平方向，垂直滚动值为：vertical，垂直滚动时容器必须指定高度
}
```
* 如果数据只有一条，文字比较短，为了有跑马灯效果，建议把同样的数据多复制几条

## 内置方法
- stop()
- play()
```js
const pmd = new lzPmd('.***', [...]);
pmd.stop(); //暂停滚动
pmd.play(); //开始滚动
```

# 更新日志
## 20231117 libing
### 增加class名称，便于自定义样式（样式不生效可以使用!important）
- lz-pmd-container 容器
- lz-pmd-item 每一条数据
- lz-pmd-point 文字前面小圆点
- lz-pmd-title 标题文字