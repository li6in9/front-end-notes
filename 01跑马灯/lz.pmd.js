class lzPmd {
    constructor(selectors, option) {
        this.scrolling = true;
        this.target = document.querySelector(selectors);
        let obj = {
            data: [],
            target: '_blank',
            time: 20,
            color: '#000',
            hover: '#f00',
            direction: 'horizontal'
        };
        if (Array.isArray(option)) {
            obj.data = option;
        } else {
            for(let v in option){
                obj[v] = option[v];
            }
            if(obj.direction === 'vertical' && !option.time) obj.time = 3000;
        }
        this.option = obj;
        this.target.style.overflow = 'hidden';
        const style = `<style>
            ${selectors} a{ color: ${obj.color}; text-decoration:none;}
            ${selectors} a:active{ color: ${obj.color}; }
            ${selectors} a:hover{ color: ${obj.hover}; }
        </style>`;
        let html = '';
        if(obj.direction === 'horizontal'){
            obj.data.forEach(e => {
                html += `<li class="lz-pmd-item" style="display: inline-block; position: relative; padding: 0 20px 0 10px;">
                    <div class="lz-pmd-point" style="width: 5px; height: 5px; position:absolute; left: 0; top: 50%; background: ${obj.color}; border-radius: 50%; margin-top: -2px; opacity: .5;"></div>
                    <a class="lz-pmd-title" href="${e.url !== '' ? e.url : 'javascript:;'}" target="${obj.target}">${e.title}</a>
                </li>`;
            });
            this.target.innerHTML = `${style}<ul class="lz-pmd-container" style="color: ${obj.color}; white-space: nowrap; margin: 0; padding: 0">${html + html}</ul>`;
            let pmdLeft = 0;
            const dataWidth = this.target.children[1].scrollWidth / 2;
            setInterval(() => {
                if (this.scrolling) {
                    pmdLeft--;
                    if (pmdLeft + dataWidth <= 0) pmdLeft = 0;
                    this.target.children[1].style.marginLeft = pmdLeft + "px";
                }
            }, this.option.time);
        }else if(obj.direction === 'vertical'){
            const h = this.target.getBoundingClientRect().height;
            let list = JSON.parse(JSON.stringify(obj.data));
            list.push(obj.data[0]);
            list.forEach(e => {
                html += `<li style="display: block; width:100%; position: relative; padding: 0; height: ${h}px; line-height: ${h}px">
                    <a href="${e.url !== '' ? e.url : 'javascript:;'}" target="${obj.target}" style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 1; -webkit-box-orient: vertical;">${e.title}</a>
                </li>`;
            });
            this.target.innerHTML = `${style}<ul style="color: ${obj.color}; margin: 0; padding: 0; height: ${h}px">${html}</ul>`;
            let pmdTop = 0;
            setInterval(() => {
                if (this.scrolling) {
                    pmdTop += h;
                    this.target.children[1].style.transitionDuration = ".7s";
                    this.target.children[1].style.marginTop = -pmdTop + "px";
                    if (pmdTop >= h*obj.data.length) {
                        setTimeout(()=>{
                            pmdTop = 0;
                            this.target.children[1].style.transitionDuration = "0s";
                            this.target.children[1].style.marginTop = "0px";
                        },700)
                    }
                }
            }, this.option.time);
        }
        this.target.addEventListener('mouseover', () => {
            // 手机端没有鼠标放上去暂停滚动的效果
            /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent) ? this.scrolling = true : this.scrolling = false;
        })
        this.target.addEventListener('mouseout', () => {
            this.scrolling = true;
        })
    }
    stop() {
        this.scrolling = false;
    }
    play() {
        this.scrolling = true;
    }
}
