class lzAds {
    constructor(selectors, option) {
        this.autoplay = true;
        this.current = 0;
        let obj = {
            data: [],
            target: '_blank',
            direction: 'left',
            time: 3000,
            duration: '1s'
        };
        if (Array.isArray(option)) {
            obj.data = option;
        } else {
            for(let v in option){
                obj[v] = option[v];
            }
        }
        this.option = obj;
        this.init(selectors);
    }
    init(selectors){
        const adsDom = document.querySelector(selectors);
        // 没有数据
        if(!this.option.data || this.option.data.length === 0) return;
        // 只有一条数据，无需滚动
        if(this.option.data.length === 1) {
            adsDom.innerHTML = `<a href="${this.option.data[0].url}" target="${this.option.target}"><img style="width:100%; height:100%; display:block;" src="${this.option.data[0].img}"></a>`;
            return;
        }
        // 多条数据滚动
        adsDom.style.overflow = 'hidden';
        if(adsDom.style.position === '') adsDom.style.position = 'relative';
        let html = '', numHtml = '';
        this.option.data.forEach((e,n) => {
            let left = '0%';
            let top = '0%';
            if(this.option.direction==='top'){
                if( n > 0 ) top = '100%';
            }else if(this.option.direction==='left'){
                if( n > 0 ) left = '100%';
            }
            html += `<a data-num="${n}" class="lz-ads-item" href="${e.url}" target="${this.option.target}" style="position: absolute; width:100%; height:100%; top: ${top}; left: ${left}">
                <img style="width:100%; height:100%; display:block;" src="${e.img}">
            </a>`;
            numHtml += `<div class="lz-ads-num-item" data-num="${n}" style="background: ${n===0 ? '#f00' : '#000'}; opacity:0.6; width:20px; height:20px; line-height:21px; font-size:12px; text-align:center; color:#fff; ${n>=1 ? 'margin-left:1px;' : ''}">${n+1}</div>`
        });
        adsDom.innerHTML = `${html}<div class="lz-ads-num-list" style="position: absolute; bottom:0; right:0; display:flex; cursor:pointer">${numHtml}</div>`;
        const numDom = document.querySelectorAll(`${selectors} .lz-ads-num-item`);
        const imgDom = document.querySelectorAll(`${selectors} .lz-ads-item`);
        numDom.forEach(e => {
            e.onclick = () => {
                this.change(numDom, imgDom, +e.dataset.num, '0s');
                this.current = +e.dataset.num;
            }
        });
        adsDom.addEventListener('mouseover', () => {
            this.autoplay = false;
        })
        adsDom.addEventListener('mouseout', () => {
            this.autoplay = true;
        })
        setInterval(() => {
            if(this.autoplay){
                this.current ++;
                if(this.current >= this.option.data.length) this.current = 0;
                this.change(numDom, imgDom, this.current);
            }
        }, this.option.time);
    }
    change(numDom, imgDom, n, click){
        // 页码变更
        numDom.forEach((e, index) => {
            if(e.classList.contains('lz-ads-num-item-active')) e.classList.remove('lz-ads-num-item-active');
            numDom[n].classList.add('lz-ads-num-item-active');
            if(n === index) {
                e.style.background = '#f00';
            }else{
                e.style.background = '#000';
            }
        });

        let isShowing = 0;
        // 左右滚动
        if(this.option.direction === 'left'){
            imgDom.forEach((e, index) => {
                if(e.classList.contains('lz-ads-item-active')) e.classList.remove('lz-ads-item-active');
                imgDom[n].classList.add('lz-ads-item-active');
                if(e.style.left === '-100%') {
                    e.style.transitionDuration = '0s';
                    e.style.left = '100%';
                }
                else if(e.style.left === '0%') {
                    isShowing = index;
                }
            });
            setTimeout(() => {
                imgDom[isShowing].style.transitionDuration = click || this.option.duration;
                click ? imgDom[isShowing].style.left = '100%' : imgDom[isShowing].style.left = '-100%';

                imgDom[n].style.transitionDuration = click || this.option.duration;
                imgDom[n].style.left = '0%';
            }, 50);
        }
        // 上下滚动
        else if(this.option.direction === 'top'){
            imgDom.forEach((e, index) => {
                if(e.classList.contains('lz-ads-item-active')) e.classList.remove('lz-ads-item-active');
                imgDom[n].classList.add('lz-ads-item-active');
                if(e.style.top === '-100%') {
                    e.style.transitionDuration = '0s';
                    e.style.top = '100%';
                }
                else if(e.style.top === '0%') {
                    isShowing = index;
                }
            });
            setTimeout(() => {
                imgDom[isShowing].style.transitionDuration = click || this.option.duration;
                click ? imgDom[isShowing].style.top = '100%' : imgDom[isShowing].style.top = '-100%';
                
                imgDom[n].style.transitionDuration = click || this.option.duration;
                imgDom[n].style.top = '0%';
            }, 50);
        }
    }
}