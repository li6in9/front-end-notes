# 轮播广告
- 鼠标放上去后会暂停轮播
- 调用方法 `new lzAds(selectors, option)`
```js
// option为数组时
const ads = new lzAds('.ads', [{img:'图片地址', url:'链接地址'}]);

// option为对象时，可以传入更多自定义参数，示例中的值为默认值，除了data，其他都为可选参数
const ads = new lzAds('.ads', {
    data: [{img:'图片地址', url:'链接地址'}], // 数据数组
    target: '_blank', // _blank新窗口打开链接，_self当前窗口打开链接，同a标签的target属性
    direction: 'left', // left左右滚动, top上下滚动
    time: 3000, // 毫秒
    duration: '1s' // css动画切换时长
})
```

# 更新日志
## 20231117 libing
### 给div增加class名称，方便自定义样式
- lz-ads-item 广告图
- lz-ads-item-active 当前展示的广告图
- lz-ads-num-list 数字容器
- lz-ads-num-item 所有数字
- lz-ads-num-item-active 当前展示的数字