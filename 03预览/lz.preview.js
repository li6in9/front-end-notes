(function(){
    if(typeof lz === 'undefined'){window.lz = {}}
    function lzpDom(selector){ return document.querySelector(selector)}
    lz.previewHtml = function(html,option,callback){
        let obj = {
            width: '92%',
            maxWidth: '800px',
            maxHeight: '80vh',
            opacity: '.8',
            type: 'html',
            close: 'top:calc(100% + 10px); left:calc(50% - 20px);'
        }
        if(option){
            for(let v in option){
                obj[v] = option[v];
            }
        }
        const domBody = lzpDom('body');
        const bodyOverflow = domBody.style.overflow;
        domBody.style.overflow = 'hidden';
        domBody.insertAdjacentHTML('afterbegin', `<div id="lzPreviewHtml" style="position: fixed; z-index: 10010; left:0; top:0; width:100%; height:100%; background: rgba(0,0,0,${obj.opacity}); backdrop-filter: blur(25px); -webkit-backdrop-filter: blur(25px); display:flex; align-items:center; justify-content: center; color: #fff;">
            <div class="lzPreviewHtml-container" style="position: relative; max-width:${obj.maxWidth}; width:${obj.width};">
                <div class="lzPreviewHtml-content" style="max-height: ${obj.maxHeight}; overflow:auto;">${html}</div>
                <svg class="lzPreviewHtml-close" style="position: absolute; ${obj.close} width: 40px; cursor:pointer;" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3088" width="48" height="48"><path d="M512 128C300.8 128 128 300.8 128 512s172.8 384 384 384 384-172.8 384-384S723.2 128 512 128zM672 627.2c12.8 12.8 12.8 32 0 44.8s-32 12.8-44.8 0L512 556.8l-115.2 115.2c-12.8 12.8-32 12.8-44.8 0s-12.8-32 0-44.8L467.2 512 352 396.8C339.2 384 339.2 364.8 352 352s32-12.8 44.8 0L512 467.2l115.2-115.2c12.8-12.8 32-12.8 44.8 0s12.8 32 0 44.8L556.8 512 672 627.2z" p-id="3089" fill="#ffffff"></path></svg>
            </div>
        </div>`)
        if(callback) callback();
        lzpDom('.lzPreviewHtml-close').onclick = function(){
            obj.type === 'video' && lzpDom('.lzPreviewVideo video').pause();
            lzpDom('#lzPreviewHtml').remove();
            domBody.style.overflow = bodyOverflow;
        }
    }
    lz.previewVideo = function(url){
        const html = `<div class="lzPreviewVideo"><video src="${url}" style="width:100%; display:block;" controls x5-video-player-fullscreen="false" x-webkit-airplay="true" x5-playsinline="true" playsinline="true" webkit-playsinline="true" x5-video-player-type="h5" preload="auto" airplay="allow"></video></div>`;
        lz.previewHtml(html, {
            type: 'video'
        },function(){
            lzpDom('.lzPreviewVideo video').play();
        })
    }
    lz.previewIframe = function(url, option){
        let obj = {
            maxWidth: '1200px',
            maxHeight: '80vh',
            type: 'iframe',
        }
        if(option){
            for(v in option){
                obj[v] = option[v];
            }
        }
        const html = `<div class="lzPreviewIframe"><iframe src="${url}" style="border:0; background:#fff; width:100%; height: ${obj.maxHeight};"></iframe></div>`;
        lz.previewHtml(html, obj)
    }

    lz.previewImage = function(url, list){
        let arr = list ? list : [url];
        const obj = {
            width: '100%',
            height: '100%'
        }
        const imgStyle = 'width:100%; height:100%; display:block;';
        const domBody = lzpDom('body');
        const bodyOverflow = domBody.style.overflow;
        domBody.style.overflow = 'hidden';

        let items = '';
        for(let i in arr){
            items += `<div id="lzPreviewImage${i}" style="${imgStyle} flex-shrink:0; scroll-snap-align: start; scroll-snap-stop: always;"><img src="${arr[i]}" style="${imgStyle} object-fit: contain;"></div>`
        }
        let html = `<div id="lzPreviewImage" style="position:fixed; width:${obj.width}; height:${obj.height}; z-index:10010; left:0; top:0; background: rgba(0,0,0,.8); backdrop-filter: blur(25px); -webkit-backdrop-filter: blur(25px); overflow-x: scroll; display:flex; scroll-snap-type: x mandatory;">
            ${items}
        </div><div id="lzPreviewImageNum" style="position:fixed; top: 15px; left:50%; z-index:10011; color:#fff; font-size:14px; background: rgba(0,0,0,.1); padding:3px 10px; border-radius:14px; transform: translateX(-50%);"></div>`;
        domBody.insertAdjacentHTML('afterbegin',html);
        lzpDom(`#lzPreviewImage${arr.indexOf(url)}`).scrollIntoView();
        var domScroll = lzpDom('#lzPreviewImage');
        var domNum = lzpDom('#lzPreviewImageNum');
        function scrolling(){
            domNum.innerText = (parseInt(domScroll.scrollLeft / domScroll.clientWidth)+1) + ' / ' + arr.length;
        }
        scrolling();
        domScroll.addEventListener('scroll', scrolling)
        domScroll.onclick = function(){
            domBody.style.overflow = bodyOverflow;
            domScroll.removeEventListener('scroll', scrolling)
            domScroll.remove();
            domNum.remove();
        }
    }
})()